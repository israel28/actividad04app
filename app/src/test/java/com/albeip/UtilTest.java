package com.albeip;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

public class UtilTest {

    @Test void muestraMaximoYMinimoTest() {
        Map<String, Integer[]> map =      new HashMap<>();
        map.put("BACALAR",                new Integer[]{245,     9,  204});
        map.put("BENITO JUÁREZ",          new Integer[]{5426, 1074, 4136});
        map.put("COZUMEL",                new Integer[]{365,    67,  271});
        map.put("FELIPE CARRILLO PUERTO", new Integer[]{317,    47,  240});
        map.put("ISLA MUJERES",           new Integer[]{194,    15,  141});
        map.put("JOSÉ MARÍA MORELOS",     new Integer[]{119,    26,   77});
        map.put("LÁZARO CÁRDENAS",        new Integer[]{267,    20,  200});
        map.put("OTHÓN P. BLANCO",        new Integer[]{3084,  174, 2251});
        map.put("PUERTO MORELOS",         new Integer[]{29,     12,   14});
        map.put("SOLIDARIDAD",            new Integer[]{1451,  176, 1198});
        map.put("TULUM",                  new Integer[]{229,    17,  188});
        Util.muestraMaximoYMinimo(map);
    }

    @Test void test() {
        System.out.println(App.metrica.infectados.ordinal());
        System.out.println(App.metrica.defunciones.ordinal());
        System.out.println(App.metrica.recuperados.ordinal());
    }
}
