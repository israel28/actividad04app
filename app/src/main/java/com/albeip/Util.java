package com.albeip;

import java.util.*;

public class Util {
    private static final String LIMPIA = "\033\143";
    private static final long ESPERA_DOS_SEGUNDOS = 2 * 1000;

    public static void limpiaPantalla() {
        System.out.println(LIMPIA);
    }

    public static void espera() throws InterruptedException {
        Thread.sleep(ESPERA_DOS_SEGUNDOS);
    }

    public static void muestraMaximoYMinimo(Map<String, Integer[]> contagiosPorMunicipio) {
        Map<Integer, List<String>> ranking = new TreeMap<>();
        for (String municipio: contagiosPorMunicipio.keySet()) {
            int contagios = contagiosPorMunicipio.get(municipio)[App.metrica.infectados.ordinal()];
            if (ranking.containsKey(contagios)) {
                ranking.get(contagios).add(municipio);
            } else {
                List<String> municipiosMismoRanking = new ArrayList<>();
                municipiosMismoRanking.add(municipio);
                ranking.put(contagios, municipiosMismoRanking);
            }
        }
        String municipioConMinimo = "";
        String municipioConMaximo = "";
        int contador = 0;
        for (Integer valorRanking: ranking.keySet()) {
            if (contador == 0) {
                // si hay varios municipios con el mismo ranking, toma solo el primero
                municipioConMinimo=ranking.get(valorRanking).get(0);
            } else if (contador == ranking.size()-1){
                // si hay varios municipios con el mismo ranking, toma solo el primero
                municipioConMaximo=ranking.get(valorRanking).get(0);
            }
            contador++;
        }
        System.out.println("El municipio que más contagios tiene es: " + municipioConMaximo);
        System.out.println("El municipio que menos contagios tiene es: " + municipioConMinimo);
    }


}
